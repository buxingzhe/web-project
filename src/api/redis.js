import request from '@/utils/request'

export function redisDemo() {
    return request({
        url: '/redisTest',
        method: 'get'
    })
}