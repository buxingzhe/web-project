import axios from 'axios';

const request = axios.create({
    baseURL: '/api',  // 设置默认的请求URL
    timeout: 10000,  // 设置超时时间
    headers: {
        'Content-Type': 'application/json'  // 设置默认的请求头
    }
});

// 添加请求拦截器
request.interceptors.request.use(function (config) {
    const isToken = (config.headers || {}).isToken === false
    console.log(isToken)
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
request.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    return response.data;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});

export default request;
